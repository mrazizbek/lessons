package lesson5;

public class Monster {
    int eye;
    int foot;
    int hand;

    Monster(int eye, int foot, int hand) {
        this.eye = eye;
        this.foot = foot;
        this.hand = hand;
    }


    Monster(int eye, int foot) {
        this.eye = eye;
        this.foot = foot;
    }

    Monster(int eye) {
        this.eye = eye;

    }

    Monster() {
        this.eye = 2;
        this.foot = 2;
        this.hand = 2;
    }

    static void voice(){
        System.out.println("Госол");
    }

    static void voice(int i){
        for (int j=1; j<=i; j++){
            System.out.println("Госол");
        }
    }
    static void voice(int i, String word){
        for (int j=1; j<=i; j++){
            System.out.println(word);
        }
    }
}

