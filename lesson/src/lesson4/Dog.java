package lesson4;

public class Dog {
    String name;
    String breed;
    int speed;

    void run() {
        for (int i = 1; i < speed; i++) {
            System.out.println("бегу");
        }
    }

    String info(String name, String breed, int speed) {
        this.name = name;
        this.breed = breed;
        this.speed = speed;
        return (name + " " + breed + " " + speed);
    }
}
